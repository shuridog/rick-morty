<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class RickMortyApiService {
    public $client;
    const RICK_MORTY_ENDPOINT = 'https://rickandmortyapi.com/api/';

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getData(string $endpoint, string $params = '') : array {
        $response = $this->client->request(
            'GET',
            self::RICK_MORTY_ENDPOINT . $endpoint . $params,
        );

        return $response->toArray();
    }

    public function convertToQueryParams(array $params) : string {
        $filter = '';
        if (empty($params)) {
            return $filter;
        }

        if (isset($params['character']) && !empty($params['character'])) {
            return $params['character'];
        }

        if (isset($params['episode']) && !empty($params['episode'])) {
            return $params['episode'];
        }

        if (isset($params['location']) && !empty($params['location'])) {
            return $params['location'];
        }

        foreach($params as $key => $val) {
            if (!empty($val)) {
                $filter = '?' . $key . '=' . $val;
            }
        } 

        return $filter;
    }

    public function getPage(string $response) : string {
        return substr($response, strpos($response, "?page=") + 6);
    }
}