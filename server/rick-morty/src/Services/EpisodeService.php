<?php

namespace App\Services;
use App\Services\RickMortyApiService;

class EpisodeService extends RickMortyApiService {
    const LOCATION_ENDPOINT = 'episode/';

    public function getEpisodeData(array $params = []) : array {
        $filter = $this->convertToQueryParams($params);
        $response = $this->getData(self::LOCATION_ENDPOINT, $filter);
        
        $arrResponse = $response;
        $pageFlag = false;

        if (empty($params['episode'])) {
            $arrResponse = $response['results'];
            $pageFlag = true;
        }

        foreach($arrResponse as $key => $val) {
            $arrResponse[$key]['characterCount'] = count($val['characters']);
            $charId = [];
            foreach($val['characters'] as $k) {
                $charId[] = substr($k, strpos($k, "/character/") + 11);
            }
            $arrResponse[$key]['characters'] = $charId;
        }

        if ($pageFlag) {
            if (!empty($response['info']['next'])) {
                $response['info']['next'] = $this->getPage($response['info']['next']);
            }
    
            if (!empty($response['info']['prev'])) {
                $response['info']['prev'] = $this->getPage($response['info']['prev']);
            }

            $response['results'] = $arrResponse;
            
            return $response;
        }
        
        return $arrResponse;
    }
}