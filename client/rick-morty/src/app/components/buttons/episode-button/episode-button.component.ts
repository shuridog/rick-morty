import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import { EpisodeService } from 'src/app/services/episode/episode.service';

@Component({
  selector: 'app-episode-button',
  templateUrl: './episode-button.component.html',
  styleUrls: ['./episode-button.component.scss']
})
export class EpisodeButtonComponent implements OnInit {
  @Output() episodeData = new EventEmitter<any>();
  constructor(private episodeService: EpisodeService) { }

  ngOnInit(): void {
  }

  getEpisode() {
    this.episodeService.getEpisode().subscribe(
      response => {
        response['type'] = 'episode';
        this.episodeData.emit(response);
    });
  }
}
