import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpisodeButtonComponent } from './episode-button.component';

describe('EpisodeButtonComponent', () => {
  let component: EpisodeButtonComponent;
  let fixture: ComponentFixture<EpisodeButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpisodeButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EpisodeButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
