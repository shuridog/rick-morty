import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingOverlayService {

  private count = 0;
  private loadingOverlay$ = new BehaviorSubject<string>('');
  constructor() { }

  getSpinnerObserver(): Observable<string> {
    return this.loadingOverlay$.asObservable();
  }

  requestStarted() {
    if (++this.count === 1) {
      this.loadingOverlay$.next('start');
    }
  }

  requestEnded() {
    if (this.count === 0 || --this.count === 0) {
      this.loadingOverlay$.next('stop');
    }
  }

  resetSpinner() {
    this.count = 0;
    this.loadingOverlay$.next('stop');
  }
}
